//
//  FontType.swift
//  VisualReminders
//
//  Created by Rizwan on 03/06/2023.
//

import Foundation

enum FontType: String {
    case bold = "Poppins Bold"
    case light = "Poppins Light"
    case medium = "Poppins Medium"
    case regular = "Poppins Regular"
    case semi_bold = "Poppins SemiBold"

}
