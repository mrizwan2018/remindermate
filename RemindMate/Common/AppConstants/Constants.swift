//
//  Constants.swift
//  RemindMate
//
//  Created by Rizwan on 05/06/2023.
//

import Foundation
struct Constants {
    static let startTxt = "Start"
    static let progressTime = "03:58"
    static let reminderListTitle = "Reminders List"
    static let startLabel = "Start"
    static let nowLbl = "Now"
    static let actionBtnMessage = "Okay"

    struct Images {
        static let settingsBtn = "settings_btn"
        static let actionImg = "tick"
        static let voiceImg = "voice_btn"
        static let closeImg = "close_btn"

    }
}
