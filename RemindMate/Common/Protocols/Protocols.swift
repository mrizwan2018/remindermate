//
//  Protocols.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import Foundation

import Combine
import SwiftUI

protocol ReminderListViewProtocol: AnyObject {
    func updateReminders(_ reminders: [Reminder])
}

protocol ReminderListRouterProtocol {
    associatedtype Content: View
    func makeDetailsView(for reminder: Reminder) -> Content
}

//riz

protocol ReminderDetailsRouterProtocol {
    associatedtype Content: View
    func makeDescriptionView() -> Content
}


protocol ReminderListInteractorProtocol {
    func fetchReminders() -> AnyPublisher<[Reminder], Error>
}

protocol ReminderListPresenterProtocol: ObservableObject {
    var reminders: [Reminder] { get }
    func loadReminders()
}

protocol ReminderDetailPresenterProtocol: ObservableObject {
    var reminder: Reminder { get }
}

protocol ProgressViewPropertiesProtocol {
    var progressTime: String { get }
    var viewWidth: CGFloat { get }
    var viewHeight: CGFloat { get }
    var cornerRadius: CGFloat { get }
    var trailingPadding: CGFloat { get }
    var fontSize: CGFloat { get }
    var labelHeight: CGFloat { get }
}

protocol TimerViewPropertiesProtocol {
    var startTime: String { get }
    var currentTime: String { get }
    var startLabel: String { get }
    var currentLabel: String { get }
    var viewWidth: CGFloat { get }
}

struct TimerViewProperties: TimerViewPropertiesProtocol {
    let startTime: String
    let currentTime: String
    let startLabel: String
    let currentLabel: String
    let viewWidth: CGFloat
}
