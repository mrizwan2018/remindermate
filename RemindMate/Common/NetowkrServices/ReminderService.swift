//
//   RemindService.swift
//  ReminderMuse
//
//  Created by Rizwan on 03/06/2023.
//

import Combine
import Foundation
import UIKit

import Network
private var monitor: NWPathMonitor?

protocol ReminderService {
    func fetchReminders() -> AnyPublisher<[Reminder], NetworkError>
}

enum ReminderAPI {
    case remindersList
}

extension ReminderAPI: WebServiceProtocol {

    var endPoint: String {
        switch self {
        case .remindersList:
            return "/v1/api/rest-test"
        }
    }
    var httpMethod: HTTPMethod {
        switch self {
        case .remindersList: return .get
        }
    }
}

open class APIReminderService: ReminderService {
    private let networkService: any NetworkServiceProtocol

    init(networkService: any NetworkServiceProtocol) {
        self.networkService = networkService
    }
    private var cancellables = Set<AnyCancellable>()

    func fetchReminders() -> AnyPublisher<[Reminder], NetworkError> {
        return NetworkMonitorService.shared.isConnectedToInternet
            .flatMap { isConnected -> AnyPublisher<[Reminder], NetworkError> in
                if isConnected {
                    print("Connected to the internet!")
                    let publisher: AnyPublisher<[Reminder], NetworkError> = self.networkService.performRequest(for: ReminderAPI.remindersList, params: nil)
                    publisher
                        .sink(receiveCompletion: { _ in },
                              receiveValue: { reminders in
                                ReminderCD.saveReminder(reminders: reminders)
                        })
                        .store(in: &self.cancellables)
                    return publisher
                } else {
                    print("Not connected to the internet.")
                    return ReminderCD.getReminders()
                }
            }
            .eraseToAnyPublisher()
    }

    deinit {
        for cancellable in cancellables {
            cancellable.cancel()
        }
    }
}
