//
//  Utilities.swift
//  RemindMate
//
//  Created by Rizwan on 05/06/2023.
//

import Foundation
struct Utility {
    static var currentTime: String {
        let timeFormatter = DateFormatter()
        timeFormatter.dateFormat = "hh:mm a"
        return timeFormatter.string(from: Date())
    }
}
