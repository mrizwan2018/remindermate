//
//  ViewModifiers.swift
//  RemindMate
//
//  Created by Rizwan on 05/06/2023.
//

import SwiftUI
import Foundation
extension View {
    func backgroundPattern() -> some View {
        self
            .background(
                Image("bg_pattern")
                    .resizable()
                    .scaledToFit()
                    .edgesIgnoringSafeArea(.all)
            )
            .padding()
    }
}
