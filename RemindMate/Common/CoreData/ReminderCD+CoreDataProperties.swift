//
//  ReminderCD+CoreDataProperties.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//
//

import Foundation
import CoreData

extension ReminderCD {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ReminderCD> {
        return NSFetchRequest<ReminderCD>(entityName: "ReminderCD")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var image: String?
    @NSManaged public var schedule: ScheduleCD?

}

extension ReminderCD: Identifiable {

}
