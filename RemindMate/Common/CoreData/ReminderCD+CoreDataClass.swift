//
//  ReminderCD+CoreDataClass.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//
//

import Foundation
import CoreData
import Combine

@objc(ReminderCD)
public class ReminderCD: NSManagedObject {
    static func saveReminder(reminders: [Reminder]) {
        self.deleteReminders()
        for reminder in reminders {
            let newreminder = ReminderCD(context: CoreDataManager.shared.context)
            newreminder.id = reminder.id
            newreminder.name = reminder.name
            newreminder.image = reminder.image

            newreminder.schedule = ScheduleCD.prepare(schedule: reminder.schedule)
        }
        CoreDataManager.shared.saveContext()
    }

    static func getReminders() -> AnyPublisher<[Reminder], NetworkError> {
        let fetchRequest: NSFetchRequest<ReminderCD> = ReminderCD.fetchRequest()

        do {
            let reminders = try CoreDataManager.shared.context.fetch(fetchRequest)
            let rems = reminders.map { Reminder(from: $0) }
            return createPublisher(from: rems)
        } catch {
            return createPublisher(from: [])
        }
    }

    private static func createPublisher(from reminders: [Reminder]) -> AnyPublisher<[Reminder], NetworkError> {
        return Just(reminders)
            .setFailureType(to: NetworkError.self)
            .eraseToAnyPublisher()
    }

    static func deleteReminders() {
        let context = CoreDataManager.shared.context
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = ReminderCD.fetchRequest()
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try context.execute(deleteRequest)
            try context.save()
        } catch {
            print("Failed to delete reminders: \(error)")
        }
    }

}
