//
//  RemindMateApp.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import SwiftUI

@main
struct RemindMateApp: App {
    // Create the services and presenter only once here
    private let apiService = APIReminderService(networkService: URLSessionService())
    private let interactor: ReminderListInteractor
    private let presenter: ReminderListPresenter

    init() {
        // Initialising the interactor and presenter
        interactor = ReminderListInteractor(apiService: apiService)
        presenter = ReminderListPresenter(interactor: interactor, router: ReminderListRouter())
    }

    var body: some Scene {
        WindowGroup {

            ReminderListView(reminderListPresenter: presenter,
                                                selectedReminder: dummyReminder
                                                )

        }
    }
}
