//
//  ReminderListPresenter.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import Foundation
import Combine

class ReminderListPresenter: ReminderListPresenterProtocol {
    @Published var reminders = [Reminder]()
    @Published var isLoading = false
    @Published var selectedName: String = ""
    private let interactor: ReminderListInteractorProtocol
    private var router: ReminderListRouter
    
    private var cancellables = Set<AnyCancellable>()
    init(interactor: ReminderListInteractorProtocol, router: ReminderListRouter) {
        self.interactor = interactor
        self.router = router
    }

    /// Load reminders using the interactor
    func loadReminders() {
        isLoading = true
        interactor.fetchReminders()
            .receive(on: DispatchQueue.main)
            .sink { [weak self] completion in
                switch completion {
                case .failure(let error):
                    print("Error: \(error)")
                    // TODO: Handle error more effectively
                case .finished:
                    self?.isLoading = false
                }
            } receiveValue: { [weak self] reminders in
                self?.reminders = reminders
                self?.selectedName = reminders.first?.name ?? ""

            }
            .store(in: &cancellables)
    }
    
    func makeDetailsView(for reminder: Reminder) -> ReminderDetailsView {
        self.router.makeDetailsView(for: reminder)
        
    }
    
    
}
