//
//  ReminderListInteractor.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import Foundation
import Combine

class ReminderListInteractor: ReminderListInteractorProtocol {
    private let apiService: APIReminderService

    init(apiService: APIReminderService) {
        self.apiService = apiService
    }

    func fetchReminders() -> AnyPublisher<[Reminder], Error> {
        apiService.fetchReminders()
            .mapError { $0 as Error }
            .eraseToAnyPublisher()
    }

}
