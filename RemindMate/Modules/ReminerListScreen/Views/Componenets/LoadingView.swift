//
//  LoadingView.swift
//  RemindMate
//
//  Created by Rizwan on 05/06/2023.
//

import SwiftUI

struct LoadingView: View {
    var body: some View {
        Text("Loading...")
    }
}
