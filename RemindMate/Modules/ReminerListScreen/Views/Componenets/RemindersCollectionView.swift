//
//  RemindersCollectionView.swift
//  RemindMate
//
//  Created by Rizwan on 05/06/2023.
//

import SwiftUI

struct RemindersCollectionView: View {
    @ObservedObject var reminderListPresenter: ReminderListPresenter
    @Binding var selectedReminder: Reminder
    @Binding var isNavigationActive: Bool
    @State var selectedReminderName: String = ""

    var body: some View {
        CollectionView(reminders: $reminderListPresenter.reminders) { selected in
            self.selectedReminder = selected
            self.isNavigationActive = true
        } onCellChange: { selected in
            selectedReminderName = selected.name ?? "No Item"
        }
    }
}
