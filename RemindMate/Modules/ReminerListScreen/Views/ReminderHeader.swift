//
//  ReminderHeader.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import SwiftUI

struct ReminderHeader: View {
    var title: String
    var time: String
    var icon: String
    private let itemWidth: CGFloat = 20

    var body: some View {
        HStack {
            Text(title)
                .fontStyle(RortyFont.title)
            Spacer()
            Text(time)
                Spacer().frame(width: 20)
                .fontStyle(RortyFont.body)

            Image(icon)
        }
    }
}
