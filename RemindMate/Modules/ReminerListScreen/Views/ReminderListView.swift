//
//  ReminderListView.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import SwiftUI

struct ReminderListView: View {
    @ObservedObject var reminderListPresenter: ReminderListPresenter
    @State var selectedReminder: Reminder
    @State var isNavigationActive: Bool = false
    @State var selectedReminderName: String = ""

    

    var body: some View {
        NavigationView {
            MainContent(reminderListPresenter: reminderListPresenter,
                        selectedReminder: $selectedReminder,
                        isNavigationActive: $isNavigationActive,
                        selectedReminderName: $selectedReminderName)
            .backgroundPattern()
                .onAppear {
                    reminderListPresenter.loadReminders()
                }
                .onReceive(reminderListPresenter.$selectedName) { selectedName in
                    self.selectedReminderName = selectedName
                }

        }.navigationViewStyle(StackNavigationViewStyle())
    }
}

struct MainContent: View {
    @ObservedObject var reminderListPresenter: ReminderListPresenter
    @Binding var selectedReminder: Reminder
    @Binding var isNavigationActive: Bool
    @Binding var selectedReminderName: String
    

    var body: some View {
        VStack {
            ReminderHeader(title: Constants.reminderListTitle, time: Utility.currentTime, icon: Constants.Images.settingsBtn)

            if !reminderListPresenter.reminders.isEmpty {
                RemindersCollectionView(reminderListPresenter: reminderListPresenter,
                                        selectedReminder: $selectedReminder,
                                        isNavigationActive: $isNavigationActive,
                                        selectedReminderName: selectedReminderName
                )
            } else {
                LoadingView()
            }

            Text(selectedReminderName)
                .fontStyle(RortyFont.title2)
            NavigationLink(destination: reminderListPresenter.makeDetailsView(for: selectedReminder), isActive: $isNavigationActive) {
                               EmptyView()
            }
            .hidden()
        }
    }
}
struct ReminderList_Previews: PreviewProvider {
        static var previews: some View {
            let interactor = ReminderListInteractor(apiService: APIReminderService(networkService: AlamofireService()))
            let presenter = ReminderListPresenter(interactor: interactor, router: ReminderListRouter())
            ReminderListView(reminderListPresenter: presenter, selectedReminder: dummyReminder)
                .previewInterfaceOrientation(.landscapeRight)
        }
}
