//
//  ReminderListRouter.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import Foundation
import UIKit

class ReminderListRouter: ReminderListRouterProtocol {
    typealias Content = ReminderDetailsView


       func makeDetailsView(for reminder: Reminder) -> ReminderDetailsView {
           ReminderDetailsView(reminder: reminder)
       }

}
