//
//  ReminderDetailsHeaderView.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import SwiftUI

struct ReminderDetailsHeaderView: View {
    var reminderName: String
    var onClose: () -> Void
    var body: some View {
        VStack {
            HStack {

                Text(reminderName)
                    .fontStyle(RortyFont.title)
                Image(Constants.Images.voiceImg)
                Spacer()
                Image(Constants.Images.closeImg)
                    .onTapGesture {
                    onClose()
                            }
            }

        }
    }
}
