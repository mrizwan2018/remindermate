//
//  ReminderDetails.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
import SwiftUI
struct ReminderDetailsView: View {
    @Environment(\.presentationMode) var presentationMode
    @State var isNavigationActiveDetailView: Bool = false
    var reminder: ReminderProtocol
    var body: some View {
        NavigationView {
            VStack(spacing: 0) {
                ReminderDetailsHeaderView(reminderName: reminder.name ?? "", onClose: {
                    self.presentationMode.wrappedValue.dismiss()
                }).padding()
                HStack(spacing: 0) {
                    ReminderDetailImageView(width: 255, height: 245, image: reminder.image ?? "")
                    ActivityCardView(reminder: reminder as! Reminder, onClose: {
                        self.isNavigationActiveDetailView = true
                        self.presentationMode.wrappedValue.dismiss()
                        
                    })
                }
                .padding(.leading)
            }
//            NavigationLink(destination: router.makeDescriptionView(), isActive: $isNavigationActiveDetailView) {
//                               EmptyView()
//            }
//            .hidden()
        }.navigationViewStyle(StackNavigationViewStyle())
        .navigationBarBackButtonHidden(true)


    }
}

struct ReminderDetails_Previews: PreviewProvider {
    static var previews: some View {
        ReminderDetailsView(reminder: dummyReminder)
            .previewInterfaceOrientation(.landscapeRight)
    }
}
let dummyReminder = Reminder(id: nil, schedule: nil, name: "Go to the dentist", image: "https://images.unsplash.com/photo-1685728399140-5650bbcfc015?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=3160&q=80")
