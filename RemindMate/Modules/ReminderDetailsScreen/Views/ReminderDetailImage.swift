//
//  ReminderDetailImageView.swift
//  RemindMate
//
//  Created by Rizwan on 04/06/2023.
//

import SwiftUI

struct ReminderDetailImageView: View {
    var width: CGFloat
    var height: CGFloat
    var image: String
    let cornerRadius: CGFloat = 10.0  // Choose the value that fits your design

    var body: some View {
        ZStack {
            AsyncImage(urlString: image)
                .frame(width: width, height: height)
                .frame(maxWidth: width, maxHeight: height)
                .clipShape(RoundedRectangle(cornerRadius: cornerRadius, style: .continuous))
        }
    }
}
