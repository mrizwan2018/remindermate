//
//  ReminderDetailPresenter.swift
//  RemindMate
//
//  Created by Rizwan on 03/06/2023.
//

import Foundation
class ReminderDetailPresenter: ReminderDetailPresenterProtocol {
    @Published var reminder: Reminder

    init(reminder: Reminder) {
        self.reminder = reminder
    }

    func okayTapped() {
        print("ok button tapped")
    }
}
