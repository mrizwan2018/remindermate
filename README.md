	
# RemindMate

RemindMate is a comprehensive reminder application project implemented using modern iOS development practices. This project is structured around the VIPER architecture pattern and showcases several best practices, such as SOLID principles, Protocol-Oriented Programming, and more.



# Project Structure
- **RemindMate/**
  - **Common/**
    - **AppConstants/**
      - Constants.swift
    - **Component/**
      - ImageLoader.swift
    - **CoreData/**
      - CoreDataManager.swift
      - **RemindMate.xcdatamodeld/**
        - **RemindMate.xcdatamodel/**
      - ReminderCD+CoreDataClass.swift
      - ReminderCD+CoreDataProperties.swift
      - ScheduleCD+CoreDataClass.swift
      - ScheduleCD+CoreDataProperties.swift
    - **Models/**
      - Reminder.swift
    - **NetowkrServices/**
      - AlamofireService.swift
      - NetworkError.swift
      - NetworkManager.swift
      - ReminderService.swift
      - URLSessionService.swift
    - **New Group/**
    - **Protocols/**
      - Protocols.swift
    - **UItilities/**
      - Common + Extension.swift
      - **Modifers/**
        - ViewModifiers.swift
      - NetworkMonitorService.swift
      - Utility.swift
  - **Main/**
    - RemindMateApp.swift
  - **Modules/**
    - **ReminderDetailsScreen/**
      - ReminderDetailInteractor.swift
      - ReminderDetailPresenter.swift
      - ReminderDetailRouter.swift
      - **Views/**
        - **CardItems/**
          - ActivityCardView.swift
          - CardActionButton.swift
          - ProgressView.swift
          - TimerView.swift
        - ReminderDetailImage.swift
        - ReminderDetails.swift
        - ReminderDetailsHeader.swift
    - **ReminerListScreen/**
      - ReminderListInteractor.swift
      - ReminderListPresenter.swift
      - ReminderListRouter.swift
      - **Views/**
        - CollectionView.swift
        - **Componenets/**
          - LoadingView.swift
          - RemindersCollectionView.swift
        - ReminderHeader.swift
        - ReminderItem.swift
        - ReminderListView.swift
  - **Preview Content/**
    - **Preview Assets.xcassets/**
  - **Resources/**
    
  - **Theme/**
    - **Fonts/**
      - Colors.swift
      - **Files/**
      - FontStyle.swift
      - FontType.swift
      - **Popins/**
        - FontViewModifier.swift
      - RortyFont.swift
- **RemindMate.xcodeproj/**
  - **project.xcworkspace/**
    - **xcshareddata/**
      - **swiftpm/**
        - **configuration/**

  
- **RemindMateTests/**
  - RemindMateTests.swift
- **RemindMateUITests/**
  - RemindMateUITests.swift
  - RemindMateUITestsLaunchTests.swift

RemindMate uses a hierarchical directory structure, divided into various categories such as `Common`, `Modules`, `Theme`, `Tests`, and more.

## Features

### SOLID Principles

The project extensively uses SOLID principles which are a set of guidelines in object-oriented programming design. They help developers write more understandable, flexible, and maintainable code.

- **Single Responsibility Principle (SRP)**
- **Open-Closed Principle (OCP)**
- **Liskov Substitution Principle (LSP)**
- **Interface Segregation Principle (ISP)**
- **Dependency Inversion Principle (DIP)**

### VIPER Architecture

VIPER (View, Interactor, Presenter, Entity, and Router) is an application of Clean Architecture to iOS projects. It is used in this project to maintain a clean and organized structure and to create a better separation of concerns, with easy-to-understand roles.

### SwiftUI

SwiftUI is used for declarative UI development in this project.

### Combine

The Combine framework is used in this project for handling asynchronous events over time, simplifying the work with asynchronous code.

### CoreData

CoreData is used in this project for persistent data storage, allowing the app to efficiently store, cache, and retrieve data.

### Protocol-Based Network Abstraction Layer

The project uses protocol-based abstraction layers in the networking component. This approach provides several benefits such as improved testability, better separation of concerns, and easier changes in the networking layer.

### Unit Testing

The project includes both unit tests and UI tests to ensure functionality correctness, using the XCTest framework.

## Modules

The `Modules` directory contains various components of the VIPER architecture for different features of the app, including `ReminderListScreen` and `ReminderDetailsScreen`. 

## Theme

The `Theme` directory contains all of the design-related components such as colors, fonts, and any other styling related files.

## Tests

The `Tests` directory contains unit tests and UI tests to ensure the correctness of the app's functionality.

## Common

The `Common` directory houses shared components like AppConstants, CoreData related files, models, network services, utilities, etc.

## Future Improvement

There is always room for improvement; in future versions of the application, we are planning to add more features and use the latest technologies and architectures to keep the application robust and maintainable. Contributions are always welcome!

## Conclusion

RemindMate is a robust, scalable, and maintainable application, thanks to the SOLID principles, VIPER architecture, and other best practices used. It provides a great starting point for anyone looking to understand or build on these principles and concepts.
